# Grid Wars

Grid Wars is a 2D strategy game. The aim of the game is to defeat all enemies in each level by spending money on troops, which can be placed before the start of the battle. There are 10 levels, with each being increasingly difficult. While the game is intended to be casual, the game also supports a high-score table, enabling players to compete to beat levels while spending as little money as possible.

To play the game, simply launch `gw_main.py`. The game is best enjoyed with a leaderboard (which requires PyMySQL and MySQL), although it is playable without these programs.

This game was a result of my A-Level Computer Science project. The full report can be found [here](https://drive.google.com/file/d/1rYjsqXZ-RG1uerVYSsLQQ5I_ZTHkjSE2/).
